﻿// Sample use
public class Demo
{
	public MainPage()
	{
		this.InitializeComponent();

		var accelerometer = Windows.Devices.Sensors.Accelerometer.GetDefault();
		if (accelerometer != null)
		{
			ShakeGestures.ShakeGesturesHelper.Instance.ShakeGesture += new EventHandler<ShakeGestures.ShakeGestureEventArgs>(Instance_ShakeGesture);
			ShakeGestures.ShakeGesturesHelper.Instance.MinimumRequiredMovesForShake = 3;
			ShakeGestures.ShakeGesturesHelper.Instance.Active = true;
		}
	}

	private async void Instance_ShakeGesture(object sender, ShakeGestures.ShakeGestureEventArgs e)
	{
		await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => txtTest.Text = "I'm all shook up!");            
	}
}